import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

/*
  Generated class for the CountryProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class CountryProvider {

  constructor(public http: HttpClient) {
  }

  getCountry(): Observable<any> {
    return this.http.get('https://restcountries.eu/rest/v2/all');
  }

  display():void {
    console.log('Hello CountryProvider Provider');
  }

}
