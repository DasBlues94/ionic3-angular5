import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the CountriesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-countries',
  templateUrl: 'countries.html',
})
export class CountriesPage {
  countryList: any;
  countryDetail: any;
  constructor(public navCtrl: NavController, public navParams: NavParams) {
    // this.countryProvider.getCountry()
    // .subscribe((response:any) => {
    //   this.countryDetail = response.filter(r => r.name.includes(this.navParams.get("countryName")));
    // });
    this.countryDetail = navParams.get("countryDetail");
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CountriesPage');
  }

}
