import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { CountryProvider } from '../../providers/country/country';
import { CountriesPage } from '../countries/countries';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  countryList: any;
  searchedCountry: string;
  temp: any;
  constructor(public navCtrl: NavController, private countryProvider: CountryProvider) {
    this.countryProvider.getCountry()
    .subscribe((response:any) => {
      console.log(response);
      this.countryList = response;
      this.temp = response;
    });
  }
  filterCountry() {
    console.log(this.searchedCountry);
    if (this.searchedCountry.trim() !== "") {
      this.countryList = this.temp.filter(c => c.name.includes(this.toTitleCase(this.searchedCountry)));
    } else {
      this.countryList = this.temp;
    }
  }
  pushPage(country:any) {
    this.navCtrl.push(CountriesPage, {
      countryDetail : country
    });
  }
  toTitleCase(str) {
    return str.replace(/\w\S*/g, function(txt){
        return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
    });
  }
}
